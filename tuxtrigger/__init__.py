#
# Copyright 2022-present Linaro Limited
#
# SPDX-License-Identifier: MIT

"""TuxTrigger Project by Linaro"""

__version__ = "0.7.0"
